package home.learning;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

public class HelloWorldTaskLet implements Tasklet{

	public RepeatStatus execute(StepContribution contribution,ChunkContext chunkContext) throws Exception {
		System.out.println("Hurray!....Vignesh!!! Spring batch is working ........");
		return RepeatStatus.FINISHED;
	}

}
